To add a suggestion,
create a pull request [here](https://codeberg.org/patched/reading-list-skp-scania)
with the suggestion added to this file,
or modify directly if you have write permissions.

Feel free to add additional data, such as links to ebook/audiobook downloads,
authors, date of publication et.c..

---

# Currently reading


---

# Possible future reading

### Arbetarklassens världsåskådning
- author: Tuure Lehén
- length: 81p A4
- [download (pdf, se)](https://www.marxistarkiv.se/klassiker/lehen/lehen-arbetarklassens_varldsaskadning.pdf)

### Klasserna och demokratin
- author: Harald Rubinstein
- length: 88p A4
- [download (pdf, se)](https://www.marxistarkiv.se/profiler/rubinstein/rubinstein-klasserna_och_demokratin.pdf)

### People's History of the United States
- author: Howard Zinn
- published: 1980, 1990
- length: 622p
- [download, (epub, en)](http://libgen.li/get.php?md5=71213a3efc4df2a6b9e09564704b5541&key=K5FRQI03YPSVT9GB&mirr=1)

### Democracy: America's deadliest export
- author: William Blum
- published: 2013
- length: 340p
-[download, (pdf, en)](https://ia600606.us.archive.org/5/items/AmericasDeadliestExportBlumWilliam/America%27s%20Deadliest%20Export-%20William%20Blum%20%5BPDF%20%26%20Epub%5D%20%5BStormRG%5D/America%27s%20Deadliest%20Export%20-%20Blum%2C%20William.pdf)

### Inventing Reality: The Politics of News Media
- author: Michael Parenti
- published: 1992
- length: 288p
- [download (epub, en)](http://libgen.rs/ads.php?md5=94783d8c2537e02be7c11c7a8ce6d4e1)

### Debt: The First 5000 Years
- author: David Graeber
- published: 2011
- length: 534p
- [download (epub, en)](http://libgen.rs/ads.php?md5=784a4d178f690858f66331408070a498)

### Patriots, Traitors and Empire
- author: Stephan Gowans
- published: 2018
- length: ~290p 
- [download (epub, en)](http://libgen.rs/item/index.php?md5=6D0F058EBBE237791F3E98F54BD7F8BC)

### The Foundation of Leninism
- author: Joseph Stalin
- published: 1953
- length: ~90p
- [webpage, en](https://www.marxists.org/reference/archive/stalin/works/1924/foundations-leninism/index.htm)
- [audiobook, en](https://www.youtube.com/watch?v=Hlx4nFxaxPw&list=PLyLqgtOQaUOv053ZzdKETOQM01hREs_ho)

### Conquest of Bread
- author: Peter Kropotkin
- published: 1907
- length: ~200p

### Mutual Aid
- author: Peter Kropotkin
- published: 1902
- length: ~240p

### Bolsheviks and the Worker's Control
- author: Maurice Brinton
- published: 1970
- length: ~90p

### Social Reform or Revolution?
- author: Rosa Luxemburg
- published: 1898 (part i), 1908 (part ii)
- length: ~120p

### Socialism: Utopian and Scientific
- author: Friedrich Engels
- published: 1880
- length: ~90p

### The Wretched of the Earth
- author: Frantz Fanon
- published: 1961
- length: ~250p

### Imperialism: The Highest Stage of Capitalism
- author: Vladimir Lenin
- published: 1917
- length: ~190p

### The Society of the Spectacle
- author: Guy Debord
- published: 1967
- length: ~160p

### The Origin of the Family, Private Property and the State
- author: Friedrich Engels
- published: 1884
- length: ~220p

### Fanged Noumena: Collected Writings 1987-2007
- author: Nick Land
- length: varied (10 to 50 pages per text)
- [download (epub)](https://1lib.sk/book/3640589/97833a)
- [download (pdf)](https://1lib.sk/book/2168155/dd0c90)

### The Advent of Netwar
- length: ~130p
- [download (pdf)](https://prantare.xyz/books/politics/advent-of-netwar.pdf)
- [more info](https://www.rand.org/pubs/monograph_reports/MR789.html)

### Revolution Betrayed
- author: Leon Trotsky
- length: ~140p (~300p for the english version, large font)
- download:
    [(sv, pdf)](https://www.marxists.org/archive/trotsky/1936/revbet/revbetray.pdf)
    [(en, pdf)](https://1lib.sk/book/938815/5a3794)

### Permanent Revolution
- author: Leon Trotsky
- length: ~80p (~300p for the english version, large font)
- download:
    [(sv, pdf)](https://marxistarkiv.se/strategi-och-taktik/den-permanenta-revolutionen)
    [(en, pdf)](https://1lib.sk/book/2578734/8e71e4)

### Materialism and Empirio-criticism
- author: Vladimir Lenin
- length: ~400p (very large font)
- download:
    [(en, epub)](https://pranta.re/books/philosophy/materialism-and-empirio-criticism.epub)
    [(en, pdf)](https://pranta.re/books/philosophy/materialism-and-empirio-criticism.pdf)

---

# Done

### Networks and Netwars
- length: ~400p
- [download (epub)](https://prantare.xyz/books/politics/networks-and-netwars.epub)
- [download (pdf)](https://prantare.xyz/books/politics/networks-and-netwars.pdf)
- [more info](https://www.rand.org/pubs/monograph_reports/MR1382.html)

### Left Wing Communism
- author: Vladimir Lenin
- published: 1920
- length: ~100p

### On Contradiction
- author: Mao Zedong
- published: 1937
- length: ~35p
- [download](https://1lib.sk/book/5240575/13ea8e)

##### Study companion
- length: ~90p
- [download](https://1lib.sk/book/5679824/88343f)

### The State and Revolution
- Author: Vladimir Lenin
- published: 1917
- Length: ~190p
- [download (epub, en)](https://prantare.xyz/books/politics/state-and-revolution.epub)

### Lenin for Beginners
- author: Richard Appignanesi
- length: ~170p (comic, not much text)
- [download (pdf, en)](https://3lib.net/book/2764843/d85ace)
