Delar av störst intresse:
Kapitel 1, 2, 3.
Kapitel 5, introduktionen och f.o.m. "The Internet's Effect on Activism".

# Kapitel 1
Starka sociala band ökar ett nätverks effektivitet.

Delat narrativ/mål av deltagande i ett nätverk kan öka sammanhållning,
t.ex. M4A, anti-imperialistiska vänstern, breadtube,
andra anarkistiska nätverk.

## Svärmning
Nätverket angriper en gemensam punkt.
Behöver ej vara hela nätet, och varje nod behöver ej göra det konstant.
Det viktiga är att ett mål ska kunna attackeras stadigt och kontinuerligt.
Ex. cancel culture.

---

Udda hybrider och symbioser är troligare mellan nätverkskrigande aktörer.
Ex. tankies och anarkister som stöttar varandra på nätet/youtube.

## Nätkrig
Samhällelligt konfliktsätt som utnyttjar nätverkad organisering.
Organisering, ideologi, strategi, teknologi o.s.v. är anpassad till
informationsåldern.

Former av nätverk (kedja, nav, tätt):

![](network-types.png)

Noder kan vara individer, grupper, organisationer, delar av organisationer,
eller t.o.m. stater.

Varje nod kan variera i storlek/"medlems"antal,
de kan vara olika tätt bundna internt
(spontan folkrörelse kontra strikt disciplinerad organisation),
och olika inkluderande.

Noderna i ett visst nätverk kan likna varandra,
eller ha en typ av "arbetsfördelning" där olika noder har olika uppgifter.

Gränsen av ett nätverk behöver inte vara tydligt definierat, men kan vara det.

De olika formerna som listades innan kan utnyttjas för olika taktiska samband,
t.ex. används kedjor för smuggeloperationer.

Traditionella hierarkier kan vara noder, t.ex. ett leninistiskt parti.

Täta nätverk har potential för nya typer av organisering i informationsåldern.
Delade principer, intressen, mål och ideologier kan fungera som klister för
sammanhållning i ett tätt nätverk.
Organisationen kan vara mer platt, då alla noder vet vad som behöver göras.

Alla noder behöver inte vara i konstant kommunkation,
men när det önskas behöver information kunna spridas ut snabbt i och runt
nätverket så brett som önskas.

Nätverk behöver inte vara bundet till internet.
Internet bör snarare ses som ett verktyg som förenklar nätverksformer av
organisering.
Organisering i verkligheten är fortfarande av störst vikt,
och internet möjliggör organiseringen av nät.

## Organisatoriska styrkor
Svärmning är en kordinerad strategisk attack mot en/några punkter från alla
håll, med kontinuerlig kraft och uthållig.
Svärmning fungerar som taktik både i fysisk konfrontation,
men även i andra sammanhang.
Battle of Seattle lyfts som ett exempel på fysiskt nätkrig.

Diversitet i nätverket gör det lättare att bemöta en bredd av skilda problem,
då noder med kunskap, erfarenhet eller specialisering kan anropas istället
för att samma organisation måste kunna hantera allt.

Ett nätverk ger redundans och robusthet. Interoperabilitet, att noder effektivt
kan jobba tillsammans trots att de är uppdelade,
tillsammans med decentraliserad kontroll,
ger nätverket skydd från att kollapsa om enskilda ledare skulle attackeras.
Noder av nätverket kan attackeras och besegras, utan att nätverket i sig
tar större skada.
Detta kan ge fiender uppfattningen av en större vinst,
när nätverket egentligen fortfarande är starkt.

Blurring är blandandet av offensiv och defensiv.
T.ex. kan en attack mot ett mål ske i namnet av självförsvar.
Det är då inte lika enkelt för utomstående att avgöra om en attack är
offensiv eller defensiv.


Kommentar:
Det är viktigt att säkerställa kommunikationskanaler med integritet,
så att inte t.ex. facebook eller google kan stoppa kommunikationen i ett
nätverk.

"It takes networks to fight networks"


# Kapitel 2
## Arketypiska nätkrig
Protagonisterna är diversa,
utspridda noder som delar gemensamma mål och intressen,
ofta ordnade på ett fullt nätat vis.
Det är mer flexibelt och anpassningsbart,
med större möjlighet att utnyttja alla medlemmars talanger.

## Grundläggande egenskaper för nätkrig
1. Kommunikation/koordination specificeras ej formellt,
utan växer fram/anpasses till situationen.
Ofta är relationerna informella.

2. Det interna nätet kompletteras med kopplingar till externa individer.

3. Interna och externa kopplingar möjliggörs främst av gemensamma
normer/värderingar, och ömsesidig tillit.

Det mesta av det interna arbetet utförs i självstyrande lag.

## Organisationer i Mellanöstern
Traditionella organisationer på 60-70-talet var byråkratiska
med en mer nationalistisk eller marxistisk agenda.
Autonoma celler fanns,
men det fanns en hierarki med tydliga raporteringsrelationer.
Nyare organisationer som Hamas, Hizbollah, och al-Qaeda
är mindre beroende av byråkrati,
och mer beroende av delade värden och horisontell kordination för att nå mål.
Olika element jobbar över och under jord,
t.ex. jobbar Hamas i moskéer för att värva medlemmar,
sprida propaganda och samla pengar.

## Teknik möjliggör nätorganisationer
1. Lägre överföringstid av information gör det möjligt för spridda aktörer att
kommunicera/kordinera.
Det har redan skett förr med telefonens uppkommst,
då stora företag kunde decentralisera till lokala brancher.

2. Lägre kostnad för kommunikation gör att informationsintensiva
organisationsupssättningar blir mer genomförbart.
Det blir möjligt med mer decentralisering och autonomi.

3. Ökad bredd och komplexitet i informationsdelning.
Chatter, gruppsamtal, forum, hemsidor är nya kommunikationsmedel
som inte har samma behov av närhet som tidigare medel.

Internet för kommunikation möjliggör ökad hastighet för mobilisering,
mer dialog medlemmar sinsemellan, vilka båda ger mer flexibilitet.
Taktiker kan tas fram och anpassas snabbare.
Det blir möjligt att skapa delgrupper, utföra en aktion,
och sedan splittra upp snabbt igen.
"Ad hoc"-organisationer för olika situationer blir alltså mer möjliga
[t.ex. en försvarsgrupp för Richard Stallman när han blev cancellad].

## Inspo från Bin Ladens nät
Använd krypterade kanaler för all kommunikation för att försvåra avlyssning
[ex. matrix istället för messenger eller discord].
Kryptera känslig data [använd tails].
Pengainsamling via nätet [använd monero där möjligt, går ej att spåra].

## Mitigerande faktorer (för fedsen)
IRL social koppling fortfarande viktigt för skapande och uppehåll av
värderingar.
Det sociala nätverket är fortfarande grunden för det elektroniska.
Alla spår som lämnas blir ett "liability" [facebook, google, okrypterade mail].
Vissa av Bin Ladens attacker stoppades via mail-avlyssning.
World Trade Center's bombare Yousefs telefonloggar blev grund för vidare
undersökning av potentiella terrorister [igen, använd tails för koms].

Hybrider av nätverk och hierarkier kan bli nödvändiga för att få balansera
flexibilitet och säkerhet.

## ITs roll i nätkrig
IT förbättrar informationsinsamling, analys, och informationsoperationer.

1. Perception management/propaganda, och informationsinsamling.

2. Attacker mot virtuella mål [online-infrastruktur, t.ex. mail och chat].

3. Fysisk förstörelse [verkar vara svårt att genomföra effektivt hittills].

Organisationer kommer bruka lättanvända system,
då man har begränsade mängder resurser att lära sig ny teknologi
[ha i åtanke när man rekommenderar teknologi för andra].

## Policies som blir rekommenderade för feds
1. Ha koll på förändringar i bruk av IT av organisationer,
speciellt om ledarna i grupper är speciellt tech-savy.

2. Rikta in på informationsflöden.
Implementera mer övervakning (Project Trailblazer ges som positivt exempel).
Bli bättre på att aktiv disruption av informationskanaler i nätverken,
plantera desinformation från till synes pålitliga källor i nätverket för att
försvaga dess integritet.
Gör det även IRL, inte bara elektroniskt.

3. Gör infrastrukturen bättre för att hindra offensiva informationsoperationer.
Anlita hackare både för försvar och retaliation.

4. Utnyttja nätorganisationer själv
("beat networked terrorists at their own game").
Mer samarbete mellan olika myndigheter och olika jurisdictions.
Counter-intelligence 21 (CI-21) plan,
en mängd reformer för att öka samarbete mellan CIA, FBI och Pentagon.
The Technical Support Working Group (TSWG),
grupp med över 100 medlemmar och 13 federal agencies vars mål är att hjälpa
med utveckling och deployment av teknologier mot terrorism.


# Kapitel 3
Hierarkier kan finnas inom nätverk, och vice versa.

Ett nätverk kan vara purposeful eller directionless
["medveten eller omedveten"].

## Sociala nätverk
Kopplingar skapas och bryts, förstärks och försvagas konstant,
leder till flexibilitet och "dynamism".
Dynamismen har möjligheten att ge bättre prestanda än hierarkier,
speciellt i sin förmåga att anpassa sig till förändringar i miljön.

Nätverk kan vara organiserade för specifika ändamål(directed network)
eller ske spontant p.g.a. sin effektivitet(transaction network).
Hybrider mellan dessa är givetvis möjliga, ena kan vara inbäddad bland
andra.

Nätverk kan vara högt strukturerade eller rätt lösa, med medlemmar som kommer
och går.
Vissa medlemmar kan vara kvar längre och ge kontinuitet och riktning till
organisationen.
Kopplingarna kan komma från djup ömsesidig respekt och tillit,
eller av slumpmässiga korta sammanträffande av intressen
[organiseringen av ett skydd för Stallman mot drevet är ett ex. på
sammanträffande].

### Några styrkor i sociala nätverk
- Nätverket kan agera clandestinely, och har lägre synlighet.
Fungerar med mindre formella band, och kan ha en mindre uppseendeväckande
profil.

- Uppdelningen[disperse] av organisationen gör den svårare att attackera.
Det är även lättare att flytta till områden med lägre risk för attack från
LE (Law Enforcement).

- Transnationella organisationer kan utnyttja skillnader i lagstiftning.

- Lättare att skapa redundans och tålighet mot försvagning.
Lättare att återbygga vid skada.

### Struktur
Nätverk kan/tenderar att ha kärnor med individer som styr ett nätverk
(speciellt directed nätverk).
Stark sammanhållning kan gå ut över möjligheten att sammla info
och att mobilisera resurser från miljön, och allmän flexibilitet,
då dessa starka band har en tendens att leda till mer isolation,
lägre vilja att expandera och grena ut i ett större nätverk.
Kärnan gör upp för dessa svagheter genom periferin.

Periferin har mindre dense interaktion, lösare relationer än i kärnan.
Detta tillåter nätverkat att agera bredare både geografiskt och socialt.
Mer extensive operations, mer diversitet i aktiviteter, bättre infoinsamling.
Periferin möjliggör att få varningar om faror tidigt,
för att snabbt kunna restrukturera i försvar.
Periferin ger bra försvar då det kan vara svårt att attackera kärnan
(som är beroende av tillit, svår att infiltrera),
periferin isolerar kärnan.

Uppdelning av periferin kan göra att attacker enbart har en lokal verkan,
snarare än över hela nätverket.
Det blir möjligt att göra av med en starkt infiltrerad del och bygga om.

Nätverk av nätverk kan skapas ["metanät"].
T.ex., turkiska droghandlare i Belgien kan köpa transporttjänster från
georgiska biltjuvar.
Deras interna flexibilitet ger kapacitet att samarbeta över nätverk.

Nätverk kan brygga laglig och olaglig verksamhet
[t.ex. Vänsterpartiet, Socialdemokratern som fick skydd från Revolutionär
Front].

Loose coupling -> high resilience.
Täta kopplingar i system är mindre stabila då störningar kan ge
kedjereaktioner.
Lösa klarar att begränsa skadan bättre [tänk gräser vs träd i en storm].
Redundans av kontakter mellan noder hjälper också stabilitet vid angrepp.
Mitigerar konsekvenser [t.ex. bolschevikernas tidningar när Lenin var i exil].

Teknologiska och sociala nätverk är skilda men har stark synergi.
En typ av teknologi som ger sociala nät mycket större styrka:
stark kryptering [använd matrix].
Det blir mycket svårare för feds att övervaka mer än metadata.

### Arbetsuppdelning
- Organisatörer, kärnan, dessa skapar styrningsmekanismen för nätet.

- Insulators, isolerar kärnan från fara (infiltration).
För direktiv ut från kärnan till periferin.
Ser till att kommunikationsflödena i periferin inte skadar kärnan.

- Kommunikatörer, ser till att kommunikationsflödet är effektivt från
en nod till nätverket i sin helhet.
För information från kärnan till periferin och tillbaka (feedback).

- Väktare(Guardians), säkerhet. Minskar sårbarhet mot externa attacker och
infiltration.
Precautions kring vilka som joiner genom bl.a. riter.
Förhindra lämnanden/minimera skada i fall av lämnande.

- Utvidgare(Extenders). Utvidgar nätet, rekrytering,
samtalar med andra nät och individer om samarbete.

- Monitors, rapportera svagheter och problem till kärnan, som kan agera.
Säkerställer anpassningsbarhet.

- Crossovers, individer som agerar i sfärer unikt för nätverket
(t.ex. korrupta politiker i ett kriminellt nätverk).
Värdefulla för informationsinsamling och annat skydd.

[Behöver inte ses strikt som roller, kan även användas som mall för vilka
aspekter ett nätverk behöver ta i beaktande].

## Attack mot nätverk
### Fed software
- Analyst's notebook
- Orion Leads
- Watson Powercase

Analyserar t.ex. interaktionsmönster.

### Metoder
Finn sårbarheter att koncentrera attacker mot.

Kritiska noder är viktiga punkter med låg redundans,
t.ex. p.g.a. en roll som har väldigt specialiserad kunskap.
Mer redundans skyddar, några extra kopplingar kan hjälpa att läka nätverket.
Icke-kritiska noder kan bli kritiska efter generell skada på nätverket.
En svårare attacktaktik är att även attacker redundanta viktiga punkter,
men kräver högre nivå av kordination.

Attackera kanter mellan nätverk, speciellt crossoverfigurer.

Attack på kärnan kan vara svårt, och möjligtvis ineffektivt ifall
periferin har blivit självgående.
Dock är det också möjligt att nätverket skadas utan möjlighet till
återhämtning.

Interna attacker, förstör tillit genom misinformation designad för att skapa
misstro.
Använd crossovers och mata med misinformation istället för att ta bort dem.
Misstron blir corossive och gör att individer kan gå mot en riktning som gör
dem mer sårbara mot attacker.

Härma nätorganisationer [ex. europol].
LE med flera joint missions.
Egmont Group, internationell samling av Financial Intelligence Units.
Attackerar finansiering (know your customer)
[övervakning av banktranskationer, spårande av kontanter i sedelräknare.
Använd t.ex. monero för att "un-tainta" pengarna (föreslog Snowden)].

[Dessa attacktaktiker bör både läsas som vad en nätorganisation måste skydda
sig mot, men även hur den kan angripa andra nätorganisationr]


# Kapitel 4
Sicillianska maffian har embraceat nätverkande internt och externt.

Bin Laden bringade ihop separata organisationer i al Qaeda.

Enandet av grupper kan ge bättre bruk av existerande resurser,
och det är lättare att lära sig av varandras erfarenheter.

WTO-protesterna (Battle of Seattle) var en lös koalition av
grupper och individer,
De använde realtidskommunikation, "intelligence" (som i CIA),
och lyckades stoppa ceremonin.
Polisen hade svårt att skilja på "bra och dåliga" protestdeltagare.


# Kapitel 5
Aktivister i USA för demokrati i Burma växte i stor utsträckning i USA
för att de har bättre teknologisk utveckling,
möjliggör effektivare organisering.

Internet är viktigt men ej en ersättning för annan typ av kommunikation
(kraftfullt tillskott).
IRL lobbying fortfarande mer effektivt.

Internet kunde ge stealth om så önskades av Burma-aktivister.

Pushen för lagar mot handel i Burma hann inte motverkas av företag.
Det mesta nätverkandet skedde ej offentligt, pushen kom som en "shock".

## Fördelar
- Billigt, praktiskt för att dela propaganda och kampanjmaterial.
Man kan kommunicera långa sträckor.

- Organisatoriskt verktyg. Kommunikation kan nå mycket fler personer,
vilket möjliggör kordinering av fler individer och förfinandet av
ideer m.h.a. öppen kommunikation och feedback.

- Info/nyheter når organisatörer snabbt, t.o.m. samma dag (eller inom timmar).

- Rapid replicering av lyckade efforts,
lättare att dela med sig av vinnande/förlorande strategier.
Den öppna kommunikationen förenklar anpassandet till lokala förhållanden.

- Tillåter val av aktivitetsgrad,
även folk som inte orkar/kan/vill bidra särskilt mycket kan fortfarande bidra
till nätverkets ändamål.

- Hjälper att sprida ens "cause" och kampanj.

## Nackdelar
- Riskabelt att vara för beroende av ett specifikt kommunikationsmedel.

- Lätt att övervaka [t.ex. fb, twitter, discord, mail].
Kan motverkas med krypterad kommunikation, [matrix],
PGP kan användas för att kryptera mail.

- Motstsåndare kan sabotera med desinformation.
De kan angripa kommunikation för att disrupta den [t.ex. med spam],
eller för att så dissension [t.ex. med falseflagging].
Kampanjer kan sidetrackas anonymt,
och den miljön kan även leda till att tid slösas på fingerpekande om feds.

- Information kan vara opålitlig då vemsomhelst kan sprida den.

- Tillgången till teknologi är ojämn [hårdvarumässigt och kunskapsmässigt].
Språk kan begränsa tillgång till kommunikation.
Finansiering kan även vara ett hinder (hemsidor o.s.v.).

- Kan ej ersätta IRL kampanjer och lobbying.
Risk för att folk fixerar sig på mediabild ["optics"], ist för faktisk praktik.


# Kapitel 6
Informationsfrihet viktigt [libgen, scihub].

Zapatistan (EZLN) fick sin styrka från ett samarbete med många NGOs,
som redan var rätt nätverkade.
Efterhand att nätkriget utvecklades mobiliserades två viktiga typer av NGOs:
(a) sakfrågeorienterade och (b) infrastrukturs/nätverksbyggande NGOs.
(a) agerar som "meddelandet", medan (b) blir "mediet".
Frågor som berör (a) kan vara mänskliga rättigheter, fred, miljö, handel...
(b) stöttar andra NGOs och aktivister i allmänhet, (typ) oavsett fråga.
De hjälper till med kommunikation, organisering av demonstrationer och
andra events, fostrar utbildning och utbytesaktiviteter NGOs emellan.

Nätkriget och dess "information operations" var en viktig del i att stoppa
"military operations" från att slå ut EZLN.
Mexikanska officials blev överväldigade av informationskriget.

---

Många sociala rörelser omdefinieras med the rise of nätverkande,
decentralisering och intervenering.
Inte bara aktiviteter är viktigt, utan även skapandet och spridandet av
kulturella koder [maga?].
En ny elektronisk fabric skapas och hjälper sammanlänka aktivister runtom i
världen.

Nätkrig är beroende av framväxten av svärmnätverk,
och svärmning sker bäst när spridda NGOs [eller noder allmänt] är nätverkade
och samarbetar på ett sätt som uppvisar "kollektiv diversitet"
och "kordinerad anarki".
NGOs har olika specialiserade intressen,
alla frågor kan bemötas/allt motstsånd kan angripas av  några element,
samtidigt som de anser sig agera i ett kollektiv som delar konvergerande
ideologiska och politiska ideal.

Svärmens beteende kan verka okontrollerad, men formas av omfattande rådfrågning
och samverkan.
Detta blir möjligt med den snabba kommunikationen som finns idag.

## Strategier
- Gör civilsamhället till frontlinjen i konflikten, anlänka till NGOs.
- Gör information till ett nyckelvapen, kräv frihet till informationstillgång,
utnyttja informations- och kommunikationsteknologier.
- Gör svärmning till ett mål och kapabilitet av egenvärde för att kunna
överväldiga mål.


# Kapitel 7
WTO-protesterna.

DAN (Direct Action Network) är/var en koalition av grupper, t.ex.
Rainforest Action Network, Art&Revolution, Ruckus Society.
De kordinerar protestträning, kommunikation och kollektiv strategi genom en
decentraliserad beslutsprocess.
Deras mål var att stänga ner WTO-möte i Seattle.

AFL-CIO var organiserade arbetare, och var adherents till Bill Clinton (??).
De skulle gå i en parad och ville ej joina DAN.

DAN planerade mer effektivt och mer realistiskt, en "people's convergence" i
tre vågor av blockader.

1. 200-300 pers i affinity groups.
Penetrera området nära mötet, blockera ett dussin strategiska korsningar
och håll ut tills förstärkning kommer.
Dessa är de mest militanta,
som kan tänka sig acceptera arrest och konfrontation.

2. Flera tuseen pers i affinity groups.
Skydda första vågen, klogga upp gatorna med sitt antal och med passivt
motstånd.
Det blev fler än DAN förväntade sig.

3. Flera tusen pers till i protester, från folk som bor där från paraden.
Denna våg var mycket större än DAN förväntade sig, var inte del av planen.

Polisens attack av residents, bl.a. med tårgas, drog ut dem till protester
tredje dagen.

DAN hadde "media special forces", som fokuserade på media och yttre
kommunikation.

indymedia.org delade information, info, länkar, tech support, webdev/design
mellan många oberoende mediagrupper.

Grupperna hade kontinuerliga uppdateringar över internet.
Stor användning av telefoner för direktkontakt.
Kommunikation hölls med oberoende media,
och video streamades direkt från platsen.
På marken mellan grupperna skedde kommunikation antingen f2f eller okrypterade
samtal.
Diffus kommunikation tillät kontinuerlig anpassning till förändrade
förutsättningar.

Arrest av ringledare var fruktlöst, då ledarskap och kommunikation var för
uppdelat i nätverket.
Kommunikationen expanderade och modifierades kontinuerligt, velket tog sig
kring polisens nedstängning av vissa kommunikationskanale.


# Kapitel 8
Olika sätt att bruka nätet för aktivism:

- Collection: Mycket info finns på nätet, insamling av information kan göras
för sina mål.
Det går att hitta andra individer och grupper mer liknande mål,
och potentiella stöttare av ens sak.
Går att ta sig runt censur [t.ex. m.h.a. tor].
Finns även info kring hur man använder nät och teknologi effektivt.

- Publication: Fler kanaler än förr för att publicera (des)information.
Billigare och eknlare att publicera än i traditionell media.

- Internet policy issues: privacy, encryption, internet governance,
information warfare är nya frågor som dyker upp i samband med spridseln av
internet.
Advocacy groups kom till att bli, ex. EFF ACP GILC EPIC CDT [FSF].

- Dialog/diskussion, kan utbilda/informera/samtala med folk, experter och
media kring frågor, både öppet och privat.
Kan användas för kordination av handling, både mellan medlemmar och med andra
grupper/individer.
Kryptering kan användas för att skydda kommunikation och lagrad information.

- Lobbying: Påtryckningskampanjer på politiker.

[ISPs kontrollerar världens nätåtkomst].


# Kapitel 9
SPIN, koncept som kom innan "nätkrig":
Segmentary, Polycentric, Integrated Network.

- Segmentary: Sammansatt av många olika grupper som växer/dör,
delas/sammanfogas, förökar sig och drar sig samman.

- Polycentric: Har flera, olika temporära och ibland konkurrerande ledare och
inflytandecentran.

- Networked: Löst, retikulerat (nätverksformat?), integrerat nätverk med flera
sammanlänkningar bl.a. i form av överlappande medlemsskap,
gemensamma aktiviteter, gemensamt läsande, och delade ideal/motståndare.

## Segmentary
En SPIN består av semiautonoma segment.
Nya segment skapas genom splittring av gamla, tillägning av nya segment,
eller genom att dela eller lägga till nya funktioner i nätverket.
Segment överlappar komplext, många är medlem i många segment samtidigt.

### Varför grupper delas
1. Personlig makt, deltagare och små/lokala grupper tar initiativ för att nå de
mål i rörelsen de anser viktiga och väntar ej på att bli tillfrågade.
Detta bidrar till skapandet av delningar över ideologi och taktik,
och kan intensifiera försök att rekrytera nya medlemmar.

2. Förexisterande delningar som tas med till gruppen,
t.ex. socioekonomiska skillnader, faktionalism och personliga konflikter.

3. Konkurrens mellan medlemmar, speciellt ledare,
för ekonomiska/politiska/sociala/psykologiska belöningar.
Skapar faktioner, realignear följare, och kan intensifiera rekryteringsförsök.

4. Ideologiska skillnader i kombination med en strävan mot ideologisk renhet.

Många blir bedrövade av delningar, medan andra omfamnar det.
De flesta splittringar sker i tillväxtfasen av en rörelse.

## Polycentric
Flera ledare/ledarcentrum, som inte beordras från högre upp.
Inte en enkel hierarki, utan snarare "heterarchic" (många hierarkier).
Ingen individ talar för hela organisationen.
De olika ledarna reflekterar meningsmotsättningar i rörelsen.
Kan försvåra ett gemensamt agerande.

## Networked
De olika grupperna i rörelsen är ej isolerade från varandra.
De skapar ett integrerat nätverk med ickehierarkiska sociala kopplingar.
Det finns delad förståelse, motståndare, identitet.
Nätverkandet tillåter utbyte av information och ideer,
och samordning av handlingar.

### Kopplingar
- Personliga relationer som familj, vänner, grannar.
Består vid splittringar.

- Resande evangelister. Rör sig mellan grupper och bygger personliga relationer
med dem de besöker.
Sprider ideologi, uppmanar till handling,
hjälper rekrytering och gruppskapande.
Förstärker tron.

- Samlingar. Konvent, konferenser, workshops, demonstrationer.
Får medlemmar att lära sig rörelsens ideologi, återupprättar relationer.

- Kommunikationsteknologier. Förstärker och utvidgar relationer,
används för att samtala/rådfråga, dela information och tolkningar.
Hjälper kordination.

## Integrerande faktorer
- Delad opposition, skapar solidaritet.
Många rörelser ser sig som underdogs mot etablissemanget.

- Delad ideologi.
Två "nivåer": alla har gemensam grundläggande beliefs/kärnteman.
Artikuleras dels i slogans.
Annan nivå: en myriad av tolkningar och olika emphasis på teman.

## Adaptiva funktioner av SPINs
SPIN tillåter snabb tillväxt och flexibel anpassning till snabbt ändrade
förutsättningar.

- Försvårar förtryck från auktoritet och opposition.
Lokala grupper överlever förgörelse av andra grupper.
Skyddar även mot knsekvenser av burnout.
Grupper kan hoppa in där andra inte klarar att fortsätta.

- Facilitates trial-and-error learning genom selektiv disavowal och emulering.
Grupper testar ideer och taktiker.
Kunskap om (miss)lyckanden sprids snabbt i nätverket.

- Promotes strävande, inovation och experimenterande i skapandet av förändring.


# Kapitel 10
Tre synsätt på nätverk: Teknologisk, social och organisatorisk.

Teknologisk: enkel syn, typ "de här grupper använder nätverkad teknologi för
att kommunicera".

Social: Analys av relationer i grupper.
Noder är individer och kopplingar är relationer.

Organisatorisk: Använder metodologi från det sociala,
men med ett annat synsätt: 
nät framväxande organisationsform från framsteg i kommunikation.
Nätverksorganisation har funnits som social organisation förr,
men den nya informationstekniksparadigmen har gett den materiella basen för att
det ska expandera till hela vår sociala struktur.
Många system har några mycket kopplade noder som agerar som nav,
med en större mängd mindre kopplade noder.
Resilient mot systemshockar.
Har regelbundna all-channel infodelning.
Skapar ett "spindelnät".

## Vad håller samman nätverk och gör dem effektiva?
### Organisatoriskt
Till vilken utsträckning är aktörer organiserade som nätverk?
Hur ser nätverken ut?

Topologier: Kedja, nav, all-channel, kombinationer/hybrider, center/periferi.
Kan innehålla strukturella hål, och bryggor mellan nätverk.
Möjligt att anpassa nät så att kommunikation mellan noder sker med färre hopp.

Arketypiskt nätkrig: array av noder, dispersed, all-channel-kopplade.
Fritt flöde av infodelning och diskussion (ICBL[?]).
Inget centralt högkvarter.

Organisationer som kräver sekretess kan behöva blanda hierari eller
cell-nätverk.
Ha många ledare spridda i nätverk som agerar kordinerat [?].

### Narrativt
Varför tar medlemmarna en nätverksform?
Varför stannar de i den formen?
Narrativ inte bara retorik, utan ett uttryck för personers erfarenheter,
intressen och värderingar.
Uttrycker identitet och tillhörande, vad som "vi" är.
Kommunicerar orsak, mening, uppdrag.
Rätt "saga" håller ihop ett löst nätverk,
och kan även brygga andra nätverk.
Bygger organisatorisk kultur.
Provokation till våld från motståndare hjälper att sprida sin egna saga.
Ger en "high ground".

### Doktrinär
Vilka doktriner finns för att bäst utnyttja nätverket?
Prestandan av multihub och all-channel kan bero på om medlemmarna har delade
principer och praktik, kan göra att de agerar "all in one mind".

Organisera nätverket utan en tydlig ledare genom att ha många,
och ta beslut genom concensus och consultation-mekanismer.

Använd svärmningsstrategier,
en myriad av spridda enheter som konvergerar på ett mål från alla håll,
som sedan återskingrar.

WTO-protesterna, DAN uppmanar till skapandet av självstående affinity groups
som har gemensama principer, mål, intressen, et.c..
Varje grupp bestämmer vilka handlingar de vill utföra.
Grupper som organiseras nära varandra delas vidare in i kluster.
I spokescouncil-möten skickas grupprepresentanter för att ta beslut
demokratiskt.

### Teknologisk infrastruktur
Vad för mönster och kapacitet för informationsflöde finns i nätverket?
Vilka teknologier stöttar det,
och hur väl passar det den organisatoriska designen?

### Sociala underpinnings
Fungerandet av nätverket beror även på hur och hur väl medlemmar personligen
känner varandra.
Tillit spelar större roll.
Akta för "free riders" utan personlig commitment och lojalitet,
något nätverk tenderar att vara extra svaga mot.

## Förhållningssätt mot nätaktörer
- Ignorera de flesta grupper för att lämna samarbete eller kamp tillgängligt i
framtiden.

- Lyft fram standarder som kommer avgöra ens stöd eller opposition mot grupper.

- Stötta fördelaktiga aktörer och dess nätverk.
